package com.demo.emailservice.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.emailservice.model.EmailModel;
import com.demo.emailservice.service.EmailService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController()
@RequestMapping("/api/v1")
public class EmailEndpoint {

	@Autowired
	EmailService emailService;

	@PostMapping("/send-email/")
	public void sendMail(@RequestBody EmailModel model) {
		emailService.sendMail(model);
	}
}
