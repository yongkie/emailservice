package com.demo.emailservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.demo.emailservice.model.EmailModel;

@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	JavaMailSender mailSender;
	
	@Value("${mail.from}")
	String fromEmail;

	@Override
	public void sendMail(EmailModel model) {
		// TODO Auto-generated method stub
		SimpleMailMessage email = new SimpleMailMessage();
		email.setFrom(fromEmail);
		email.setTo(model.getMailTo());
		email.setSubject(model.getSubject());
		email.setText(model.getBodyMessage());
		mailSender.send(email);
	}
}
