package com.demo.emailservice.service;

import com.demo.emailservice.model.EmailModel;

public interface EmailService {
	void sendMail(EmailModel model);
}
